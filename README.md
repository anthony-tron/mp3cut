# mp3cut

A mp3 file splitter helpful to cut a single mp3 file into multiple files.

## Example

```txt
// chapters.txt
0:00 Song 1
2:34 Song 2
4:09 Song 3
```

```shell
mp3cut -o outdir chapters.txt source.mp3
```

```shell
out
├── Song 1.mp3
├── Song 2.mp3
└── Song 3.mp3
```

## API

`Chapter`
```rust
let chapters_data = std::fs::read_to_string("chapters.txt").unwrap();

let chapters = Chapter::from(chapters_data).unwrap();
// [{ title: "Song 1", timestamp: 0:00 },
//  { title: "Song 2", timestamp: 2:34 },
//  { title: "Song 3", timestamp: 4:09 }]
```

`find_mp3_bounds`
```rust
let mp3_data = std::fs::read("source.mp3");
    
let bounds = mp3cut::find_mp3_bounds(&mp3_data, &chapters);
// [23_442, 449_396, 889_292, 1_234_567]
```
