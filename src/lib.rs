use crate::durable::Durable;

mod chapter;
mod durable;

pub use chapter::Chapter;

/// Returns the byte positions where each song begins and the position where the audio ends
pub fn find_mp3_bounds(data: &Vec<u8>, chapters: &Vec<Chapter>) -> Vec<usize> {
    let mut i = 0;

    let mut decoder = rmp3::Decoder::new(&data);

    let mut duration = std::time::Duration::new(0, 0);
    let mut start = 0;
    let mut end = 0;
    let mut bounds: Vec<usize> = Vec::new();

    while let Some(frame) = decoder.peek() {
        if let rmp3::Frame::Audio(audio) = frame {
            if start == 0 {
                start = decoder.position();
            }

            end = decoder.position() + audio.source().len();

            duration += audio.duration();

            if chapters.len() != i + 1 && duration.as_secs() >= chapters[i + 1].as_secs() as u64 {
                bounds.push(start);

                // reset
                start = 0;
                i += 1;
            }

            decoder.skip();
        }
    }

    bounds.push(start);
    bounds.push(end);

    return bounds;
}
