use clap::{arg, command};
use std::borrow::Borrow;
use std::env;
use std::fs::{create_dir_all, read, read_to_string, write};
use std::process::exit;

fn main() {
    let app = command!()
        .arg(arg!(-o --out <outdir> "out dir").required(true))
        .arg(arg!(<chapters> "chapters file"))
        .arg(arg!(<mp3> "mp3 file to cut"));

    let matches = app.get_matches();

    if let Some(version) = matches.value_of("version") {
        println!("{}", version);
        return;
    }

    let chapters_path = matches.value_of("chapters").unwrap();
    let mp3_path = matches.value_of("mp3").unwrap();
    let outdir = matches.value_of("out").unwrap();

    if let Err(error) = create_dir_all(outdir) {
        println!("Could not create dir `{}` ({})", outdir, error);
        exit(1);
    };

    let data = read(mp3_path).expect("Music file not found");

    let chapters_data = read_to_string(chapters_path).expect("Chapters file not found");
    let chapters =
        mp3cut::Chapter::from(chapters_data.borrow()).expect("Cannot parse chapters file");

    let bounds = mp3cut::find_mp3_bounds(&data, &chapters);

    for j in 0..bounds.len() - 1 {
        let start = bounds[j];
        let end = bounds[j + 1];

        write(
            format!("{}/{}.mp3", outdir, chapters[j].title),
            &data[start..end],
        )
        .expect("Failed to write file");
    }
}
