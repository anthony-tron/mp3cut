use rmp3::Audio;
use std::time::Duration;

pub trait Durable {
    fn duration(&self) -> Duration;
}

impl Durable for Audio<'_, '_> {
    fn duration(&self) -> Duration {
        Duration::from_secs_f32(self.sample_count() as f32 / self.sample_rate() as f32)
    }
}
